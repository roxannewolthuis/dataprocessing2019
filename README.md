## Course dataprocessing - Exercises and final assignment

* **Author:**            	Roxanne Wolthuis
* **Student number:**      	347958
* **Class:**                BFV3
* **Course:**               Dataprocessing
* **Study:**                Bio-informatics
* **Institute:**            Institute for Lifescience & Technology
* **Lector:**               Fenna Feenstra
* **Commissioned by:**      Fenna Feenstra
* **Date of start:**        14 - 02 - 2019

## Content of this repository
* **Opdrachten:**
A folder with all 5 class exercises and the final assignment.
* Opdracht 1 
* Opdracht 2
* Opdracht 3
* Opdracht 4
* Opdracht 5
* Eindopdracht 
	* In this folder you will find everything that was used for creating the final assignment. A more in dept view of the final assignment can be found by the head: 'Content of Eindopdracht'

## Content of eindopdracht
* **data:**
A directory with all used data samples and the indexes: beeIndex and blastIndex, the human index has to be created by the user because this file is to big to add to bitbucket. Please check th Prerequisites for this.
* **scripts:**
A directory with all created scripts that are used in the new pipeline
* **Snakefiles**
A directory with all snakefiles except the main snakefile
* **Snakefile**
The main snakefile
* **config.yaml**
The configuration file

## Prerequisites
* Snakemake version 4.6. The workflow management system tool that is used to build this pipeline.
https://snakemake.readthedocs.io/en/stable/
* Python version3.5. Python is the scripting language used in rule qualityFilter and sizeFilter.
https://www.python.org/downloads/
* Bowtie2 version 2.3.0.
http://bowtie-bio.sourceforge.net/bowtie2/index.shtml
* Blastn version2.6.0
https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download
* velvetoptimiser version 2.2.5
https://github.com/tseemann/VelvetOptimiser/wiki/VelvetOptimiser-Manual
* clustalo 1.2.4
http://www.clustal.org/omega/
* Create a human index
The humand index can be downloaded here https://www.ncbi.nlm.nih.gov/genome/guide/human/.
To create the index you can use bowtie2-build. An explanation on how to use bowtie2-build can be found here: http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#the-bowtie2-build-indexer
Save the human index in the directory 'Eindopdracht/data/HumanIndex' with the name human.

## Installation and usage of the pipeline
To use this pipeline be sure that you run the pipeline in a virtual environment with all prerequisites.
Running this pipeline can be done by the following command: snakemake --snakefile Snakefile. 
Where Snakefile is the main file. No other arguments or installation is neccessary.

## Explanation of the Rules.
* **Rule all:**
* **humanMapping:**
Bowtie2 is used in this rule as a negative filtering tool. Bowtie2 maps the original reads(input as fastq) against the human genome. The output that will be generated is a general SAM-file and an additional fastq file with reads that did not map against the human genome. The SAM-file is not used in this pipeline. The fastq file with the leftover reads will be used in the beeMapping rule.

* **beeMapping:**
Bowtie2 is used in this rule as a negative filtering tool aswell. The rule works the same as the humanMapping rule but this rule uses the leftover reads from the humanMapping as input. Bowtie2 maps the left over reads against the Apis Mellifera genome.The output that will be generated is a general SAM-file and an additional fastq file with reads that did not map against the Apis Mellifera genome. The SAM-file is not used in this pipeline. The fastq file with the leftover reads will be used in the qualityFilter rule.

* **blastn:**
Blastn was used to blast the original reads(input as fasta) against a Deformed Wing Virus genome for filtering the reads. Blastn generates an XML file as output that will be used in the qualityFilter rule.

* **qualityFilter:**
The qualityFilter rule runs a python script that is partially rewritten from the original pipeline. The script loops through the XML file(output of blastn) and Fastq(output of beeMapping) file and filters reads with a Quality score of 0.8 or higher into a new fastq file. These high quality reads will be used as input in the sizeFilter rule.

* **sizeFilter:**
The sizeFilter rule runs a python script that is partially rewritten from the original pipeline. The script filters all the high quality reads with a length above 1000 bp. These reads are added to new file that is used in the velvetAssembly rule.

* **velvetAssembly:**
This rule runs velvetoptimiser to assembly a possible Deformed Wing Virus genome or parts of it based on the filtered data from sizeFilter. The output is a folder for each file containing multiple files. The file that will be used in this pipeline is the contigs.fa file.

* **clustaloMSA:**
Clustalo is used in this rule to run a Multiple sequence alignment. Clustalo takes the contigs.fa file from velvetAssembly as input file and creates two output files. The general output file that is created is a .fastq file with the sequences, this file is not being used in the pipeline. The other output file is a .tree file that used for visualisation in the visualizeTree rule.

* **visualizeTree:**
THis rule was supposed to run a R script and to create a tree with the package ape and ggtree, but due to an error this rule calls a python script. The python script is a replacement for the visualisation and creates a .txt file. In this txt file a tree is shown with possible virus reads. This is the final output of the pipeline.

## The project description
The purpose of this project is to improve a part of the original minion pipeline. The original pipeline can be found here: https://bitbucket.org/LisettevDam/deformed-wing-virus-analysis-in-bee-blood/src/master/. The 

## Result information
all output files that are not used are kept in case there might be an interest in these files. The final output is a visualisation and can be found in the output directory: visualisation.

## Issues
1) By rule qualityFilter multiple inputs are used in the python script as snakemake.input[0] and snakemake.input[1], however snakemake don't seem to work with the right order, so i build a check in the python script. This also happens with the snakemake.output in the clustalo script.
2) Due to low quality test data the following rules only run on barcode03 of the example files
* velvet_assembly
* clustalo_msa
* visualize_tree
3) Due to a bug in R(package Ape) it is not possible to create a tree with the function read.tree. The function is not working so the code cannot proceed to create a tree visualisation. In order to fix this problem i used a script that uses Phylo.draw_ascii() to draw a tree into a .txt file
4) Sometimes velvetoptimiser crashes random about the coverage cutoff due to low quality test data, this problem can be solved by simply rerunning the script(i could not find another fix)
5) Sometimes a directory will be locked when you try to rerun the pipeline, you will get this Error: Directory cannot be locked. Please make sure that no other Snakemake process is trying to create the same files in the following directory ... This error can be fixed by running the command snakemake --unlock and then run the command for the script again
""""This is the visualize file of the pipeline.
In this file all the rules will visualize the filtered data.
This is the final rule of the pipeline and generates the final output"""

rule visualize_tree:
	""""This rule was supposed to run a R script and to create a tree with the package ape and ggtree,
	but due to an error this rule calls a python script. 
	The python script is a replacement for the visualisation and creates a .txt file. 
	In this txt file a tree is shown with possible virus reads.
	This is the final output of the pipeline."""
	input:
		guideTree = "output/clustalo/tree/barcode0{sample}.tree"
	output:
		visual = "output/visualisation/barcode0{sample}.txt"
	message: "Running a script to visualize the output of the MSA in a tree in a txt file"
	script:
		"scripts/visualizeTree.py"

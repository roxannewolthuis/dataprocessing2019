# hier verder
	def find_read_fastq(self):
		""""function that filters the reads above a lenght of 1000bp
		input: fastq file with reads above 0.8% alignment score
		output: fastq file with reads length above 1000 bp
		"""

		#~ os.chdir("..")
		#~ os.system("ls -l")
		#~ os.system("mkdir -p long_reads")
		#~ os.chdir("long_reads")

		# for fastq files in high_quality_reads
		for self.fastqfile in glob.glob('../high_quality_reads/*'):
			filename = self.fastqfile[22:39] + "_long_reads.fastq"
			file = open(filename, "w")
			with open("{}".format(self.fastqfile), "r") as handle:
				for record in SeqIO.parse(handle, "fastq"):
					if len(record.seq) >= 1000:
						file.write(record.format("fastq"))
			file.close()

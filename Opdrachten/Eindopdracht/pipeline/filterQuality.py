import sys
class MinIONpipeline:
	def __init__(self):
		"""constructor function"""
		pass

	def get_sample_files_in_lists(self):
		""""function that puts the .fastq and .xml files into lists for comparing
		input: xml and fastq files
		output: xml and fastq files in separate lists"""
		
		# create new lists
		self.xmlfiles = []
		self.fastqfiles = []
		
		# Get xml and fasta files and append the files to lists
		#input = xml files --> output/blast/*
		for self.xml_file in glob.glob(snakemake.input[0] + "*"):
			self.xmlfiles.append(self.xml_file)
		#input = fastq files --> output/mapping/failedReads/bee/*
		for self.fastq_file in glob.glob(snakemake.input[1] + "*"):
				self.fastqfiles.append(self.fastq_file)


	def reads_filter(self):
		"""function that filters the reads on an alignment score above 0.8%
		input: blast output in xml format and porechop output in fastq format
		output: fastq file with an alignment score above 0.8%
		"""

		#~ os.system("mkdir -p assembly")
		#~ os.chdir("assembly")
		#~ os.system("mkdir -p high_quality_reads")
		#~ os.chdir("high_quality_reads")

		# for file in the lists if the name of the files are the same run the sequence parser
		for item in self.xmlfiles:
			for self.i in self.fastqfiles:
				# TODOOOO - NAAM CHECKEN
				if item[6:23] == self.i[31:48]:
					# # Create files as result_handle and fasta_result
					# input snakemake --> snakemake input XML
					self.result_handle = open(input.snakemake[0]"{}".format(item), "r")
					# for line in self.result_handle:
					# SNAKEMAKE OUTPUT --> HighQualityFastq
					self.filename = snakemake.output[0] + item[6:23] + "_highQuality.fastq"
					file = open(self.filename, "w")
					blast_records = NCBIXML.parse(self.result_handle)
					t = []
					unique_list = []
					# For the record in all the records
					for blast_record in blast_records:
						# print(blast_record.query)
						for alignment in blast_record.alignments:
							# For the hsp in the hsps of the alignment
							for hsp in alignment.hsps:
								#print(hsp.align_length)
								align_score = hsp.identities / hsp.align_length
								# print(align_score)
								# INPUT SNAKEMAKE fastqpath
								with open(snakemake.input[1] + "{}".format(self.i), "r") as handle:
									for record in SeqIO.parse(handle, "fastq"):
										if align_score > 0.8 and record.description == blast_record.query:
											#print(align_score)
											# #print("de record in fasta bestand is: {} \n de record sequentie in het fasta bestand is: {} \n de blast query  van xml is:{} \n".format(record.description, record.seq, blast_record.query))
											newread = record.format("fastq")
											#newread = ">{}\n{}\n".format(record.description, record.seq)
											#print(newread)
											t.append(newread)
					for x in t:
						# check if exists in unique_list or not
						if x not in unique_list:
							unique_list.append(x)
					# for every unique item put in new list
					for x in unique_list:
						#print(x)
						file.write(str(x))
					file.close()


def main():
	m = MinIONpipeline()
	m.get_assembly_files()
	m.reads_filter()
	m.find_read_fastq()
	m.fastq_converter_fasta()


if __name__ == '__main__':
	main()

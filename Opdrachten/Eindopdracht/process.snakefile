""""This is the process file of the pipeline.
In this file all the rules will process the input data so it can be used in the filter file."""


rule human_mapping:
	""""Bowtie2 is used in this rule as a negative filtering tool.
	Bowtie2 maps the original reads(input as fastq) against the human genome.
	The output that will be generated is a general SAM-file and an additional fastq file with reads that did not map against the human genome.
	The SAM-file is not used in this pipeline. The fastq file with the leftover reads will be used in the beeMapping rule."""

	input:
		samples = config["workingPath"] + config["fastqPath"] + "/barcode0{sample}_trimmed.fastq"
	output:
		SAM_human = "output/mapping/samfiles/human/" + "human_barcode0{sample}.sam",
		leftOverReads_human = "output/mapping/failedReads/human/" + "barcode0{sample}.fastq"
	message: "Mapping original reads against the human genome to filter the reads"
	threads: 6
	shell:
		"bowtie2 -U {input.samples} -x {config[workingPath]}{config[humanIndexPath]} -S {output.SAM_human}  -p {threads} --un {output.leftOverReads_human}"

rule bee_mapping:
	""""Bowtie2 is used in this rule as a negative filtering tool aswell.
	The rule works the same as the humanMapping rule but this rule uses the leftover reads from the humanMapping as input.
	Bowtie2 maps the left over reads against the Apis Mellifera genome.
	The output that will be generated is a general SAM-file and an additional fastq file with reads that did not map against the Apis Mellifera genome.
	The SAM-file is not used in this pipeline. The fastq file with the leftover reads will be used in the qualityFilter rule."""
	input:
		leftOverReads_human = "output/mapping/failedReads/human/" + "barcode0{sample}.fastq"
	output:
		SAM_bee = "output/mapping/samfiles/bee/" + "barcode0{sample}.sam",
		leftOverReads_bee = "output/mapping/failedReads/bee/" + "barcode0{sample}.fastq"
	message: "Mapping leftOverReads_human against the bee genome to filter the reads"
	threads: 6
	shell:
		"bowtie2 -U {input.leftOverReads_human} -x {config[workingPath]}{config[beeIndexPath]} -S {output.SAM_bee}  -p {threads} --un {output.leftOverReads_bee}"

rule blastn:
	""""Blastn was used to blast the original reads(input as fasta) against a Deformed Wing Virus genome for filtering the reads.
	Blastn generates an XML file as output that will be used in the qualityFilter rule."""
	input:
		samples = config["workingPath"] + config["fastaPath"] + "barcode0{sample}_trimmed.fasta"
	output:
		XML = "output/blast/" + "barcode0{sample}.xml" 
	message: "Blasting original reads against a full DWV genome from NCBI for filtering"
	threads: 6
	shell:
		"blastn -query {input.samples} -db {config[blastIndexPath]}  -out {output.XML} -outfmt 5 -num_threads {threads}"


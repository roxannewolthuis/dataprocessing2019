""""This is the assemble file of the pipeline.
In this file all the rules will assemble the filtered data so it can be used in the visualize file."""

rule velvet_assembly:
	""""This rule runs velvetoptimiser to assembly a possible Deformed Wing Virus genome or parts of it based on the filtered data from sizeFilter.
	The output is a folder for each file containing multiple files.
	The file that will be used in this pipeline is the contigs.fa file."""
	input:
		bigReads = "output/sizeFilter/barcode0{sample}.fastq"
	output:
		assembledReads = "output/assembly/barcode0{sample}"
	threads: 6
	message: "Running velvet assembly program for each barcode"
	shell:
		"velvetoptimiser --f '-fastq -short {input.bigReads}' --k --c --t {threads} --d {output.assembledReads}"

rule clustalo_msa:
	""""Clustalo is used in this rule to run a Multiple sequence alignment(via a script).
	Clustalo takes the contigs.fa file from velvetAssembly as input file and creates two output files.
	The general output file that is created is a .fastq file with the sequences, this file is not being used in the pipeline.
	The other output file is a .tree file that used for visualisation in the visualizeTree rule."""

	input:
		assembledReads = "output/assembly/barcode0{sample}"
	output:
		guideTree = "output/clustalo/tree/barcode0{sample}.tree",
		clustalOPhy = "output/clustalo/phy/barcode0{sample}.fastq"
	message: "Running Multiple sequence alignment on the assembled reads to generate .tree file for visualisation"
	threads: 6
	script:
		"scripts/clustalo.py"

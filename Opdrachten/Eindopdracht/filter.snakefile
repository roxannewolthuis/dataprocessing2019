""""This is the filter file of the pipeline.
In this file all the rules will filter the processed data so it can be used in the assemble file."""

rule qualityFilter:
	""""The qualityFilter rule runs a python script that is partially rewritten from the original pipeline.
	The script loops through the XML file(output of blastn) and Fastq(output of beeMapping) file and filters reads with a Quality score of 0.8 or higher into a new fastq file.
	These high quality reads will be used as input in the sizeFilter rule."""
	input:
		XML = "output/blast/" + "barcode0{sample}.xml",
		leftOverReads_bee = "output/mapping/failedReads/bee/" + "barcode0{sample}.fastq"
	output:
		highQualityReads = "output/qualityFilter/barcode0{sample}.fastq"
	message: "Filtering the reads depending on quality score, relocating high quality reads to new directory"
	script:
		"scripts/filterQuality.py"

rule sizeFilter:
	""""The sizeFilter rule runs a python script that is partially rewritten from the original pipeline.
	The script filters all the high quality reads with a length above 1000 bp.
	These reads are added to new file that is used in the velvetAssembly rule."""
	input:
		highQualityReads = "output/qualityFilter/barcode0{sample}.fastq"
	output:
		bigReads = "output/sizeFilter/barcode0{sample}.fastq"
	message: "Filtering the reads depending on size, relocating big reads to new directory"
	script:
		"scripts/filterSize.py"

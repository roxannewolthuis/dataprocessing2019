import sys
from Bio import Phylo

class visualizeTree():
	def __init__(self):
		"""constructor function"""
		pass


	def phylogenetic_tree(self):
		"""function that makes an phylogenetic tree from the msa data
		input: msa fastq file
		output: phylogenetic tree
		"""				
		visualizeFile = snakemake.input[0]
		newfile = open(snakemake.output[0], "w")
		tree = Phylo.read(visualizeFile, "newick")
		Phylo.draw_ascii(tree, file=newfile)
		newfile.close()


def main():
	v = visualizeTree()
	v.phylogenetic_tree()
	return 0

if __name__ == '__main__':
	main()


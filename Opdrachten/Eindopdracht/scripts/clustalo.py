import sys
import os

class clustalo():
	def __init__(self):
		"""constructor function"""
		pass

	def clust(self):
			"""This function runs clustalo and takes care of multiple directory issues caused by velvet
			"""
			# create input name
			barcodeDir = snakemake.input[0] + "/contigs.fa"
			# check output paths
			if "fastq" in snakemake.output[0]:
				fastq = snakemake.output[0]
				tree = snakemake.output[1]
			else:
				fastq = snakemake.output[1]
				tree = snakemake.output[0]
			# run clustalo
			command = os.system("clustalo -i {} -o {} --outfmt phylip --guidetree-out {} --threads 6 -v".format(barcodeDir, fastq, tree))


def main():
	c = clustalo()
	c.clust()
	return 0

if __name__ == '__main__':
	main()

import sys
import glob, os
from Bio.Blast import NCBIXML
from Bio import SeqIO


class filterQuality():
	def __init__(self):
		"""constructor function"""
		pass


	def reads_filter(self):
		"""function that filters the reads on an alignment score above 0.8%
		input: blast output in xml format and porechop output in fastq format
		output: fastq file with an alignment score above 0.8%
		"""
		# check if filepaths are correct
		if "xml" in snakemake.input[0]:
			xml = snakemake.input[0]
			fastq = snakemake.input[1]
		else:
			xml = snakemake.input[1]
			fastq = snakemake.input[0]			

		# Create new file and name file the same as the barcode files to save high quality read
		newfilename = xml[13:22] + ".fastq" 
		output = open(snakemake.output[0],"w") 

		# open and parse xml file
		self.result_handle = open(xml, "r")
		blast_records = NCBIXML.parse(self.result_handle)
		
		# Create empty lists to save the data before adding it to a file
		t = []
		unique_list = []
		# For the record in all the records
		for blast_record in blast_records:
			for alignment in blast_record.alignments:
					  # For the hsp in the hsps of the alignment
						for hsp in alignment.hsps:
							# calculate alignmentscore
							align_score = hsp.identities / hsp.align_length
							with open(fastq, "r") as handle:
								for record in SeqIO.parse(handle, "fastq"):	
									#print(align_score)
									if align_score > 0.80 and record.description == blast_record.query:
										newread = record.format("fastq")
										t.append(newread)
										
		for x in t:
			# check if exists in unique_list or not
			if x not in unique_list:
				unique_list.append(x)

		for x in unique_list:
			print(x, file=output)
		output.close() 
		return output
							


def main():
	q = filterQuality()
	#q.get_sample_files_in_lists()
	q.reads_filter()
	return 0

if __name__ == '__main__':
	main()

import sys
import glob
from Bio import SeqIO
class filterSize():
	def find_read_fastq(self):
		""""function that filters the reads above a lenght of 1000bp
		input: fastq file with reads above 0.8% alignment score
		output: fastq file with reads length above 1000 bp
		"""
		# get input and output from snakemake
		fastq = snakemake.input[0]
		output = open(snakemake.output[0],"w")

		with open(fastq, "r") as handle:
			for record in SeqIO.parse(handle, "fastq"):
				# if record > 1000 add to new file
				if len(record.seq) >= 1000:
					print(record.format("fastq"),file=output)
		output.close()

def main():
	s = filterSize()
	s.find_read_fastq()


if __name__ == '__main__':
	main()
